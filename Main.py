from __future__ import annotations

import math, operator
from functools import *
from dataclasses import dataclass, field
from typing import List, Callable, Any, Hashable

import copy, time, random
import numpy as np
from scipy.integrate._ivp import solve_ivp

from matplotlib import pyplot as plt, animation
from matplotlib.gridspec import GridSpec
from matplotlib.widgets import *
from matplotlib.table import CustomCell
import matplotlib.cm as cm



random.seed(0)
np.random.seed(0)


###################################################################################################################

# BASE CLASSES

def LMA_merging(qts, as_in, cs_in):
    res = 1.
    for q in qts:
        res *= cs_in[q] ** as_in[q] / math.factorial(as_in[q])
    return res

def MAX_merging(qts, as_in, cs_in):
    res = np.inf
    for q in qts:
        res = min(res, cs_in[q] / as_in[q])
    res = max(res, 0.)
    return res

def MIN_merging(qts, as_in, cs_in):
    return np.maximum(np.sum(cs_in / as_in - 1.) + 1., 0.)  # TODO, probably false


@dataclass
class Quantity:
    name: str
    color: str = None
    def __eq__(self, other):
        return isinstance(other, Quantity) and self.name == other.name
    def __hash__(self):
        return id(self)


@dataclass
class Reaction:
    inputs: Hashable[Quantity, int]
    outputs: Hashable[Quantity, int]
    rate: float
    merging_aux: Callable[[List[Quantity], Hashable[Quantity, int], Hashable[Quantity, float]], float] = LMA_merging
    name: str = ""
    def __post_init__(self):
        self.merging = lambda cs: self.merging_aux(list(self.inputs.keys()), self.inputs, cs)
        self.merging: Callable[[Hashable[Quantity, float]], float]

    def get_ODE(self, qs: List[Quantity]):
        ins, outs = {q: 0 for q in qs}, {q: 0 for q in qs}
        ins.update(self.inputs), outs.update(self.outputs)
        res = np.array([outs[q] - ins[q] for i, q in enumerate(qs)])
        return lambda cs: res * self.rate * self.merging(dict(zip(qs, cs)))


@dataclass
class Model:
    c0s: Hashable[Quantity, float]
    reacs: List[Reaction]
    def __post_init__(self):
        self.qts = list(self.c0s.keys())
        self.x0 = np.fromiter(self.c0s.values(), dtype=float)

    def get_ODE(self):
        return lambda cs: sum(map(lambda r: r.get_ODE(self.qts)(cs), self.reacs))

    def integrate(self, tf):
        return solve_ivp(lambda t, x: self.get_ODE()(x), [0., tf], self.x0, method="RK45", dense_output=True)

###################################################################################################################

# PLOTTING

def plot_model(M: Model, tf, N):
    t = np.linspace(0, tf, N)
    sol = M.integrate(tf)
    x = sol.sol(t)
    for i in range(len(x)):
        plt.plot(t, x[i, :], label=M.qts[i].name, color=M.qts[i].color)
    plt.xlabel('t')
    plt.legend()
    plt.show()


###################################################################################################################

# MODELS LIST

def choose_model(model: str) -> tuple(Model, float, int):
    if model == "G_Exp":
        EC = Quantity("EColi", "green")
        EG = Reaction({EC: 1}, {EC: 2}, 1.)
        return Model({EC: 0.05}, [EG]), 100., int(1E4)
    elif model == "G_LogSimple":
        EC, øEC = Quantity("EColi", "green"), Quantity("øEColi", "black")
        LG = Reaction({EC: 1, øEC: 1}, {EC: 2}, 1.)
        return Model({EC: 0.05, øEC: 0.95}, [LG]), 100., int(1E4)
    elif model == "G_LogCouple":
        EC, øEC = Quantity("EColi", "green"), Quantity("øEColi", "black")
        LG = Reaction({EC: 2, øEC: 1}, {EC: 3}, 1.)
        return Model({EC: 0.05, øEC: 0.95}, [LG]), 100., int(1E4)
    elif model == "G_Photosynthesis":
        S, øS, E = Quantity("Surface", "green"), Quantity("øSurface", "lightgrey"), Quantity("Energy", "red")
        Extending = Reaction({øS: 1, E: 1}, {S: 1}, 1.)
        Collecting = Reaction({S: 1}, {S: 1, E: 1}, 0.1)
        return Model({S: 0.05, øS: 0.95, E: 0.}, [Extending, Collecting]), 100., int(1E4)
    elif model == "G_PhotosynthesisCompet":
        S1, S2, øS = Quantity("Surface_1", "lightgreen"), Quantity("Surface_2", "lightcoral"), Quantity("øSurface", "lightgrey")
        E1, E2 = Quantity("Energy_1", "green"), Quantity("Energy_2", "red")
        Extending_1 = Reaction({øS: 1, E1: 1}, {S1: 1}, 1.)
        Collecting_1 = Reaction({S1: 1}, {S1: 1, E1: 1}, 0.1)
        Extending_2 = Reaction({øS: 1, E2: 1}, {S2: 1}, 1.)
        Collecting_2 = Reaction({S2: 1}, {S2: 1, E2: 1}, 0.1)
        return Model({S1: 0.05, S2: 0.2, øS: 0.75, E1: 0., E2: 0.}, [Extending_1, Collecting_1, Extending_2, Collecting_2]), 100., int(1E4)

    elif model == "W_Basic":
        H, N, øN = Quantity("Humans", "red"), Quantity("Resources", "green"), Quantity("øResources", "lightgrey")
        Recovering = Reaction({N: 1, øN: 1}, {N: 2}, 0.1)
        Reproducing = Reaction({H: 1, N: 1}, {H: 2, øN: 1}, 0.3)
        Death = Reaction({H: 1}, {}, 0.1)
        return Model({H: 0.01, N: 1., øN: 0.}, [Recovering, Reproducing, Death]), 2000, int(1E4)
    elif model == "W_Human":
        H, N, øN, V = Quantity("Humans", "red"), Quantity("Resources", "green"), Quantity("øResources", "lightgrey"), Quantity("Value", "blue")
        Recovering = Reaction({N: 1, øN: 1}, {N: 2}, 0.1)
        Working = Reaction({H: 1, N: 1}, {H: 1, øN: 1, V: 1}, 0.3)  # interesting rates : 0.3, 2.0
        Reproducing = Reaction({H: 1, V: 1}, {H: 2}, 0.3, MAX_merging)
        Death = Reaction({H: 1}, {}, 0.1)
        Depletion = Reaction({V: 1}, {}, 0.1)
        return Model({H: 0.01, N: 1., øN: 0., V: 0.}, [Recovering, Working, Reproducing, Death, Depletion]), 1000, int(1E4)
    elif model == "W_Robot":
        R, N, øN, V = Quantity("Robots", "red"), Quantity("Resources", "green"), Quantity("øResources", "lightgrey"), Quantity("Value", "blue")
        Recovering = Reaction({N: 1, øN: 1}, {N: 2}, 1.)
        Working = Reaction({R: 1, N: 1}, {R: 1, øN: 1, V: 10}, 3.)
        Reproducing = Reaction({R: 1, V: 1}, {R: 2}, 3., MAX_merging)
        Death = Reaction({R: 1}, {}, 0.01)
        Depletion = Reaction({V: 1}, {}, 0.1)
        return Model({R: 0.01, N: 1., øN: 0., V: 0.}, [Recovering, Working, Reproducing, Death, Depletion]), 2000, int(1E4)

    elif model == "World3":
        P = Quantity("Population", "red")
        C = Quantity("Capital", "blue")
        A = Quantity("Agriculture", "green")  # agricultural infrastructures
        R = Quantity("Non-renewable Resources", "lightgreen")
        W = Quantity("Waste", "darkgrey")
        Investing = Reaction({P: 1, R: 1, C: 1}, {P: 1, R: 1, C: 2}, 1.)
        CapDepletion = Reaction({C: 1}, {W: 1}, 0.2)
        Reproducing = Reaction({P: 1, C: 1, A: 1}, {P: 2, C: 1, A: 1, W: 1}, 0.2)
        Dying = Reaction({P: 1}, {}, 0.1)
        FarmsBuilding = Reaction({P: 1, C: 1, R: 1}, {P: 1, A: 1}, 0.3)
        Polluting = Reaction({W: 1, A: 1}, {W: 2}, 0.1)
        PolDepleting = Reaction({W: 1}, {}, 0.01)
        reactions = [Investing, CapDepletion, Reproducing, Dying, FarmsBuilding, Polluting, PolDepleting]
        return Model({P: 0.5, C: 1., A: 0.5, R: 2., W: 1.}, reactions), 50., int(1E4)

    else:
        print("Sorry, unknown model")


tic = time.perf_counter()
M, tf, N = choose_model("G_PhotosynthesisCompet")

print(M.get_ODE()(M.c0s.values()))
plot_model(M, tf, N)
print(time.perf_counter() - tic)

